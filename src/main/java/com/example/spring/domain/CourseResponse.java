package com.example.spring.domain;

public class CourseResponse {

    private Course course;
    private Operation operation;

    public Course getCourse() {
	return course;
    }

    public void setCourse(Course course) {
	this.course = course;
    }

    public Operation getOperation() {
	return operation;
    }

    public void setOperation(Operation operation) {
	this.operation = operation;
    }

}
