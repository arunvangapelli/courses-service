package com.example.spring.domain;

import java.util.List;

import com.example.spring.error.Error;

public class Operation {

    private String correlationId;
    private List<Error> errors;

    public Operation() {
    }

    public Operation(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
