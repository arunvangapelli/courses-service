package com.example.spring.dao;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.spring.domain.Course;
import com.example.spring.entity.CourseEntity;
import com.example.spring.repository.CourseRepository;

@Component
public class CourseDao {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Course getCourse(String courseId) {
	Optional<CourseEntity> courseEntity = courseRepository.findById(Long.valueOf(courseId));
	if(!courseEntity.isPresent()){
	    return null;
	}
	return modelMapper.map(courseEntity.get(), Course.class);

    }

}
