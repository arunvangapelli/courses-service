package com.example.spring.constants;

public class ApplicationConstants {
    
    private ApplicationConstants() {
	//disallow instantiation.
    }
    
    public static final String COURSE_ID_FIELD = "courseId";

}
