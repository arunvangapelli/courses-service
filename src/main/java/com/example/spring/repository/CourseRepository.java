package com.example.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.spring.entity.CourseEntity;

@Repository
public interface CourseRepository extends CrudRepository<CourseEntity, Long>{

}
