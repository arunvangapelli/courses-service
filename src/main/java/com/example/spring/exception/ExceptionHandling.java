package com.example.spring.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.spring.error.ErrorHelper;
import com.example.spring.error.OperationsError;
import com.example.spring.exception.domain.InvalidDataFormatException;
import com.example.spring.exception.domain.ResourceNotFoundException;
import com.example.spring.localization.MessageHandler;

import brave.Tracer;

@ControllerAdvice
public class ExceptionHandling {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandling.class);
    @Autowired
    private Tracer tracer;
    @Autowired
    private ErrorHelper errorHelper;
    @Autowired
    private MessageHandler messageHandler;

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = InvalidDataFormatException.class)
    @ResponseBody
    public OperationsError handleInvalidDataFormatException(final HttpServletRequest request,
	    final InvalidDataFormatException ex) {
	LOGGER.error("InvalidDataFormatException exception occurred ", ex);
	return errorHelper.errorResponse("pathParameterInvalid", tracer.currentSpan().context().traceIdString(),
		"Unable to process path parameter.", ex.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseBody
    public OperationsError handleResourceNotFoundException(final HttpServletRequest request,
	    final ResourceNotFoundException ex) {
	LOGGER.error("Resource not found exception occurred ", ex);
	return errorHelper.errorResponse("resourceNotFound", tracer.currentSpan().context().traceIdString(),
		messageHandler.localizeErrorMessage("resourceNotFound"), ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public OperationsError handleMethodArgumentNotValidException(final HttpServletRequest request,
	    final MethodArgumentNotValidException ex) {
	List<String> validationErrors = new ArrayList<>();

	validationErrors.addAll(ex.getBindingResult().getAllErrors().stream()
		.map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()));

	return errorHelper.errorResponse(validationErrors, tracer.currentSpan().context().traceIdString());
    }
}
