package com.example.spring.exception.domain;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2886735360078459210L;

    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
