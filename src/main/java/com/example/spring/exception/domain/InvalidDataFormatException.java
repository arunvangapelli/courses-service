package com.example.spring.exception.domain;

public class InvalidDataFormatException extends RuntimeException {

    private static final long serialVersionUID = 4163238378624272681L;

    public InvalidDataFormatException() {
    }

    public InvalidDataFormatException(String message) {
        super(message);
    }
}
