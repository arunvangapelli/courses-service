package com.example.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.spring.domain.CourseResponse;
import com.example.spring.domain.Operation;
import com.example.spring.service.CourseService;

import brave.Tracer;

@Controller
@RequestMapping("/example/v1/courses")
public class CourseController {

    @Autowired
    private CourseService courseService;
    @Autowired
    private Tracer tracer;

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String saveCourse() {
	return null;

    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public CourseResponse getCourse(@PathVariable("id") String id) {
	CourseResponse courseResponse = new CourseResponse();
	courseResponse.setCourse(courseService.getCourse(id));
	courseResponse.setOperation(new Operation(tracer.currentSpan().context().traceIdString()));
	return courseResponse;
    }

}
