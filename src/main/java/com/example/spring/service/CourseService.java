package com.example.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.constants.ApplicationConstants;
import com.example.spring.dao.CourseDao;
import com.example.spring.domain.Course;
import com.example.spring.exception.domain.ResourceNotFoundException;

@Service
public class CourseService {

    @Autowired
    private CourseDao courseDao;

    public Course getCourse(String courseId) {
	
	Course course = courseDao.getCourse(courseId);
	if(course == null) {
	    throw new ResourceNotFoundException(ApplicationConstants.COURSE_ID_FIELD);
	}
	return course;
    }

}
